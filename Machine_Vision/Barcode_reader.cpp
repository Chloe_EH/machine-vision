#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <iostream>

#include "zbar.h" 

using namespace cv;
using namespace std;
using namespace zbar; 

int detect_barcode( Mat& , vector<Mat>& );
int decode_barcode(Mat& crop);

int main( int argc, char** argv )
{
	if( argc != 2)
    {
     cout <<" Usage: display_image ImageToLoadAndDisplay" << endl;
     return -1;
    }

	Mat src;
	src = imread(argv[1], CV_LOAD_IMAGE_UNCHANGED );   // Read the file

	// Check for invalid input
    if(! src.data )                              
    {
        cout <<  "Could not open or find the image" << std::endl ;
        return -1;
    }

	vector<Mat> crop;
	detect_barcode(src, crop);
	for(int i=0; i < int(crop.size()); i++)
		decode_barcode(crop[i]);

	return 0;
}

int detect_barcode( Mat& src, vector<Mat>& crop )
{
    Mat gray;

	/// Convert it to gray
	cvtColor( src, gray, CV_BGR2GRAY );

	 /// Generate grad_x and grad_y
	Mat grad_x, grad_y;
	Mat abs_grad_x, abs_grad_y;

	int scale = 1;
	int delta = 0;
	int ddepth = CV_32F;

	// Gradient X
	Sobel( gray, grad_x, ddepth, 1, 0, 3, scale, delta, BORDER_DEFAULT);
	convertScaleAbs( grad_x, abs_grad_x );
	// Gradient Y
	Sobel( gray, grad_y, ddepth, 0, 1, 3, scale, delta, BORDER_DEFAULT);
	convertScaleAbs( grad_y, abs_grad_y );

	// Total Gradient (approximate)
	Mat grad;
	addWeighted( abs_grad_x, 0.5, abs_grad_y, 0.5, 0, grad);
	
	// Threshold
	threshold( grad, grad, 10, 255, THRESH_OTSU);
 
	// Find Contours
	vector<vector<Point>> vecContours;
	vector<Vec4i> hierarchy;
	findContours(grad, vecContours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, Point(0, 0));
   
	int someAreaInPixels = int(grad.rows*grad.cols * 0.1);
	int area = 0, max_area = 0;
	vector<RotatedRect> minRect;
	for (size_t i = 0; i < vecContours.size(); i++)
	{   
		 //filter unwanted objects (contours with less than 4 points, contours with too small area)
		if (vecContours[i].size() < 4 || contourArea(vecContours[i]) < someAreaInPixels)
			continue;
		
		minRect.push_back( minAreaRect(vecContours.at(i)));
		//drawContours(src, vecContours, i, color, LINE_4, 8, hierarchy);
	}
	
	for(int i = 0; i < int(minRect.size()); i++){

		// Draw lines around barcode
		Point2f rect_points[4]; 
		minRect[i].points( rect_points );
		for( int j = 0; j < 4; j++ )
			line( src, rect_points[j], rect_points[(j+1)%4], Scalar(255,0,0), 1, 8 );

		// Rotate images
		Mat rotated;

		if(minRect[i].size.width < minRect[i].size.height)
		{
			std::swap(minRect[i].size.width ,minRect[i].size.height);
			minRect[i].angle +=90;
		}

		Mat cropped;
		// Get the rotation matrix
		Mat M = getRotationMatrix2D(minRect[i].center, minRect[i].angle, 1.0);
		// Perform the affine transformation
		warpAffine(gray, rotated, M, gray.size(), INTER_CUBIC);
		// Crop the resulting image
		getRectSubPix(rotated, minRect[i].size, minRect[i].center, cropped);
		crop.push_back(cropped);		
	}

	//imshow("crop", cropped);		
	//imshow( "test", src );

	// Wait for a keystroke in the window
    //waitKey(0);                                          
    return 0;
}
 
 
 int decode_barcode(Mat& crop)
 {
	ImageScanner scanner;  
    scanner.set_config(ZBAR_NONE, ZBAR_CFG_ENABLE, 1);  
      
    int width = crop.cols;  
    int height = crop.rows;  
	uchar *raw = (uchar *)crop.data;  
	 
	// wrap image data  
	Image image(width, height, "Y800", raw, width * height);  
	// scan the image for barcodes  
	int n = scanner.scan(image);
	
	// extract results  
	for(Image::SymbolIterator symbol = image.symbol_begin();  
     symbol != image.symbol_end();  
     ++symbol)

	{
		vector<Point> vp;  
		// Do something useful with results  
		 cout << "decoded " << symbol->get_type_name()  
			<< " symbol " << symbol->get_data() << '"' <<" "<< endl;  
           int n = symbol->get_location_size();  
           for(int i=0;i<n;i++){  
                vp.push_back(Point(symbol->get_location_x(i),symbol->get_location_y(i))); 
           }  
           RotatedRect r = minAreaRect(vp);  
           Point2f pts[4];  
           r.points(pts);  
           for(int i=0;i<4;i++){  
                line(crop,pts[i],pts[(i+1)%4],Scalar(255,0,0),3);  
           }  
           cout<<"Angle: "<<r.angle<<endl;  
	}

	imshow("imgout.jpg",crop);

	// Clean up  
	image.set_data(NULL, 0);  
	waitKey();  
	return 0;
 }  

  //imshow("threshold", thresholded);
	/*
	GaussianBlur( image, blur, Size(5,5), 0);
	imshow("blur", blur);
  /// Canny detector
    //Canny( detected_edges, detected_edges, 3, 3*3, 3 );
	Canny(blur, canny, 3, 3, 3);
	//	 GaussianBlur( image, src, Size(3,3), 0, 0, BORDER_DEFAULT );

	GaussianBlur( canny, canny, Size(9,9), 0);
	threshold( canny, canny, 10, 255, 0 );
	vector<vector<Point> > contours;
	vector<Vec4i> hierarchy;
//	findContours( canny, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, Point(0, 0) );
//	Mat drawing = Mat::zeros( canny.size(), CV_8UC3 );
//    for( int i = 0; i< contours.size(); i++ )
//     {
////Scalar color = Scalar( rng.uniform(0, 255), rng.uniform(0,255), rng.uniform(0,255) );
//       drawContours( drawing, contours, i, Scalar(0,255,255), 2, 8, hierarchy, 0, Point() );
//     }
	//vector<Vec4i> lines;
	//HoughLinesP(canny, lines, 1, CV_PI/30, 40, 40, 10 );
	//for( size_t i = 0; i < lines.size(); i++ )
	//{
	//  Vec4i l = lines[i];
	//  line( image, Point(l[0], l[1]), Point(l[2], l[3]), Scalar(0,255,255), 3, CV_AA);
	//}
    namedWindow( "Display window", WINDOW_AUTOSIZE );// Create a window for display.
    imshow( "Display window", image );                   // Show our image inside it.
	imshow( "Canny", canny ); 
	//imshow("contour", drawing);
	*/
	//Rect boundRect;
	//boundRect = boundingRect( vecContours[index]);
	//Scalar color( rand()&255, rand()&255, rand()&255 );
	////rectangle( image, boundRect.tl(), boundRect.br(), color, 2, 8, 0 );
		

