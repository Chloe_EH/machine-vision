// LibraryWrapper.h
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include "zbar.h" 

#pragma once

using namespace System;
using namespace cv;
using namespace std;
using namespace zbar; 

namespace LibraryWrapper {

	public ref class BarcodeReader
	{
		public:
			int width;
			int height;

			char* data;

			BarcodeReader(int n_width, int n_height)
			{
				width = n_width;
				height = n_height;
				data = new char[width * height];
			};

			string read_barcode();
			int detect_barcode( Mat& src, vector<Mat>& crop );
			string decode_barcode(Mat& crop);
		};
}
